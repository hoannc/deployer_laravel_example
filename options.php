<?php
namespace Deployer;
/**
 * Created by PhpStorm.
 * User: hocvt
 * Date: 11/8/17
 * Time: 08:17
 */

use Symfony\Component\Console\Input\InputOption;

option('git_server', 'g', InputOption::VALUE_OPTIONAL, 'Git server', '');

task('check_git_server', function(){
	writeln('Checking custom git server');
	$git_server = Deployer::get()->getInput()->hasOption('git_server') ? Deployer::get()->getInput()->getOption('git_server') : "gitlab";
	if($git_server){
		if($other_repo = get('repository_' . $git_server, false)){
			set('repository', $other_repo);
		}else{
			throw new \Exception("Cài đặt repository_" . $git_server . " để có thể sử dụng git server khác mặc định");
		}
	}
});

before('deploy:update_code', 'check_git_server');