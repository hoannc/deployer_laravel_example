<?php

namespace Deployer;

/**
 * Created by PhpStorm.
 * User: hocvt
 * Date: 12/22/16
 * Time: 13:50
 */

// Upload env/config file
task('config:upload', function(){
	upload('./{{hostname}}/.env', '{{release_path}}/.env');
//	upload('./{{hostname}}/server.key', '{{release_path}}/server.key');
	upload('./{{hostname}}/.bash_login', '{{release_path}}/.bash_login');
});
task('config:update', function(){
	upload('./{{hostname}}/.env', '{{current_path}}/.env');
//	upload('./{{hostname}}/server.key', '{{current_path}}/server.key');
	$output = run('{{bin/php}} {{current_path}}/artisan cache:clear');
	writeln('<info>' . $output . '</info>');
	$output = run('{{bin/php}} {{current_path}}/artisan config:cache');
	writeln('<info>' . $output . '</info>');
});
task('opcache:clear', function(){
    $output = run('{{bin/php}} {{current_path}}/artisan opcache:clear');
    $output = run('{{bin/php}} {{current_path}}/artisan opcache:optimize');
    writeln('<info>' . $output . '</info>');
});

task('npm:run:prod', function(){
    $output = run("cd {{release_path}} && {{bin/npm}} run prod");
    writeln('<info>' . $output . '</info>');
});

//task('opcache:clear', function() {
//    $output = run('{{bin/php}} {{deploy_path}}/cachetool.phar opcache:reset --web --web-path={{current_path}}/public --web-url={{web_url}}');
//    writeln('<info>' . $output . '</info>');
//});
//task('pm2:upload', function(){
//	upload('./{{hostname}}/pm2.yml', '{{deploy_path}}/pm2.yml');
//});


task('upload:file', function (){
//    run('mkdir -p {{deploy_path}}/shared/storage/files');
//    upload('{{hostname}}/cates.json', '{{deploy_path}}/shared/storage/files/cates.json');
    upload('{{hostname}}/dvhcvn.xlsx', '{{deploy_path}}/shared/storage/files/dvhcvn.xlsx');
    upload('{{hostname}}/topic.xlsx', '{{deploy_path}}/shared/storage/files/topic.xlsx');
    upload('{{hostname}}/univer.xlsx', '{{deploy_path}}/shared/storage/files/univer.xlsx');
});