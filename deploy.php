<?php
namespace Deployer;

use Symfony\Component\Console\Input\InputOption;

require 'recipe/laravel.php';
require 'recipe/npm.php';

// Configuration

set('repository', 'git@gitlab.com:hoannc/laravel_deployer.git');
//set('branch', 'deploy');
set('branch', 'master');

set('ssh_type', 'native');
set('ssh_multiplexing', true);


//set('writable_mode', 'chmod');
//set('writable_chmod_mode', '0777');

// Overide shareded_files config
set('shared_files', [

]);

// Servers

require 'servers.php';

require 'my_tasks.php';

require 'options.php';

desc('Deploying CungHocVui');


task('deploy:chv',[
	'deploy:prepare',
	'deploy:lock',
	'deploy:release',
	'deploy:update_code',
	'npm:install',
	'npm:run:prod',
	'config:upload',// custom task
	'deploy:shared',
	'deploy:vendors',
	'deploy:writable',
	'artisan:migrate',
	'artisan:storage:link',
	'artisan:view:clear',
	'artisan:cache:clear',
	'artisan:config:cache',
	'opcache:clear', //custom task
	'artisan:optimize',
	'deploy:symlink',
	'deploy:unlock',
	'cleanup',
	'success',
]);
task('deploy:continue',[
    'artisan:migrate',
    'artisan:view:clear',
    'artisan:cache:clear',
    'artisan:config:cache',
    'artisan:optimize',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success',

]);

task('deploy:npm',[
    'npm:run'
]);
before('deploy:prepare', 'show_des');

task('show_des', function(){
	writeln("Deploying {{hostname}} branch {{branch}}");
});

task('continue', [
	'success',
]);
//after('deploy:update_code', 'npm:install');
after('deploy:failed', 'deploy:unlock');