<?php

namespace Deployer;

/**
 * Created by PhpStorm.
 * User: hocvt
 * Date: 3/29/17
 * Time: 17:49
 */
host('cunghocvui')
	->hostname('118.70.176.191')
	->user('ssh_hocvt')
	->port(2016)
	->multiplexing(true)
	->set('ssh_multiplexing', true)
	->set('env_vars', ' source {{release_path}}/.bash_login && ')
	->set('config_dir', 'cunghocvui')
	->set('deploy_path', '/home/ssh_hocvt/cunghocvui.com')
	->set('bin/php', '/usr/local/php726_fpm/bin/php');

host('gc')
    ->hostname('35.185.182.142')
//    ->hostname('35.240.187.6')
    ->user('ssh_chv')
    ->port(22)
    ->multiplexing(true)
    ->set('ssh_multiplexing', true)
    ->set('env_vars', ' source {{release_path}}/.bash_login && ')
    ->set('config_dir', 'gc')
    ->set('deploy_path', '/var/www/cunghocvui.com')
    ->set('bin/php', '/usr/local/php725_fpm/bin/php')
    ->set('web_url', 'https://cunghocvui.com');

host('dev')
    ->hostname('118.70.13.36')
    ->user('answer')
    ->port(6022)
    ->multiplexing(true)
    ->set('ssh_multiplexing', true)
//    ->set('env_vars', ' source {{release_path}}/.bash_login && ')
    ->set('config_dir', 'dev')
    ->set('deploy_path', '/home/answer/Documents/colombo/cunghocvui/cunghocvui_deploy');

localhost('local')
    ->set('config_dir', 'local')
    ->set('deploy_path','/Applications/MAMP/htdocs/colombo/miny_project/deploy');
